/*
 * net.c
 *
 *  This module creates two different tasks. One task to handle a FTP server
 *  and one task to handle an OSOTB server.
 *  The FTP server will receive a firmware binary to be flashed and the OSOTB
 *  server can process commands from a host.
 *
 *  Created on: May 24, 2021
 *      Author: Julian Rodriguez aka Marifante
 *		Email: jnrodriguezz@hotmail.com
 *		Gitlab: https://gitlab.com/Marifante
 *		Github: https://github.com/Marifante
 */


// Include --------------------------------------------------------------------
#include "net_private.h" 	/* !< Net module private include. */

// Macros & constants ---------------------------------------------------------

// Private functions declaration ----------------------------------------------

// Private variables declaration ----------------------------------------------

// Public functions definition  -----------------------------------------------
/**
 * @brief init net of application.
 * @return RET_OK if success.
 */
TaskHandle_t net_init(void *arg)
{
	ret_code_t ret = RET_OK;

	/* First init the TCP-IP API. */
	ret = (NULL == API_TCPIP_Init(NULL)) ? RET_ERR_NULL_POINTER : RET_OK;

	if( RET_OK == ret )
	{
		ret = net_ftp_init();
		ret = net_osotb_init();
	}
	return NULL;
}

/*
 * @brief each server created runs with the same task function template.
 * @param arg the parameter passed by the xTaskCreate function must be a pointer
 * to a boo_net_server_t struct. This struct will hold all the functions related with
 * the server.
 * */
void net_task_template(void *arg)
{
	net_msg_t msg;
	net_server_t *sv = NULL;
	sv = (net_server_t*) arg;
	while(true)
	{
		if(pdTRUE == xQueueReceive(sv->task.cmd_queue, (uint8_t*)&msg, sv->task.queue_timeout))
		{
			net_process_msg(&msg); // process incoming message
		}
		api_fsm_work(&(sv->fsm), NULL); // Process the actual state of the fsm
	}
}
