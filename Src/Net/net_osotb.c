/*
 * net_osotb.c
 *
 *  Created on: Aug 18, 2021
 *      Author: Julian Rodriguez aka Marifante
 *		Email: jnrodriguezz@hotmail.com
 *		Gitlab: https://gitlab.com/Marifante
 *		Github: https://github.com/Marifante
 */

// Include --------------------------------------------------------------------
#include "net_private.h" 	/* !< Net module private include. */

// Macros & Constants ---------------------------------------------------------
#define NET_OSOTB_TASK_DELAY_MS			50			/*<! in ms, delay of the task. */
#define NET_OSOTB_TASK_STACK_SIZE		1024
#define NET_OSOTB_TASK_PRIOTY			osPriorityNormal
#define NET_OSOTB_QUEUE_TIMEOUT			(10 / portTICK_PERIOD_MS)
#define NET_OSOTB_QUEUE_LEN				10

/* Network config. TODO move to another place? */
#define NET_OSOTB_SERVER_PORT				49152
#define NET_OSOTB_LOG_ENABLED				1		/*<! Enables module log. */
#define	LOG_TAG						"net_osotb"	/*<! Tag assigned to logs for this module. */

#if NET_OSOTB_LOG_ENABLED
	#define NET_OSOTB_LOG_ERROR(format, ...)		LOG_ERROR(LOG_TAG, format, ##__VA_ARGS__)
	#define NET_OSOTB_LOG_WARNING(format, ...)	LOG_WARNING(LOG_TAG, format, ##__VA_ARGS__)
	#define NET_OSOTB_LOG_INFO(format, ...)		LOG_INFO(LOG_TAG, format, ##__VA_ARGS__)
	#define NET_OSOTB_LOG_DEBUG(format, ...)		LOG_DEBUG(LOG_TAG, format, ##__VA_ARGS__)
#else
	#define NET_OSOTB_LOG_ERROR(...)
	#define NET_OSOTB_LOG_WARNING(...)
	#define NET_OSOTB_LOG_INFO(...)
	#define NET_OSOTB_LOG_DEBUG(...)
#endif

// Private functions declaration ----------------------------------------------
ret_code_t net_osotb_fsm_sv_work_ongoing_handler(void *arg);
ret_code_t net_osotb_fsm_sv_create_ongoing_handler(void *arg);
ret_code_t net_osotb_fsm_sv_send_ongoing_handler(void *arg);

/* Commands rx handlers: */
ret_code_t net_osotb_rx_handler_get_uptime(char *payload, char *payload_response);
ret_code_t net_osotb_rx_handler_get_def_app_slot(char *payload, char *payload_response);
ret_code_t net_osotb_rx_handler_put_def_app_slot(char *payload, char *payload_response);
ret_code_t net_osotb_rx_handler_get_op_mode(char *payload, char *payload_response);
ret_code_t net_osotb_rx_handler_put_op_mode(char *payload, char *payload_response);

// Private variables declaration ----------------------------------------------
/* States of the fsm of this module: */
api_fsm_state_t net_osotb_fsm_states[] =
{
	API_FSM_DEFINE_STATE(	NET_FSM_SV_DISCONNECTED,
							NULL,
							NULL,
							NULL),
	API_FSM_DEFINE_STATE(	NET_FSM_SV_CREATE,
							NULL,
							net_osotb_fsm_sv_create_ongoing_handler,
							NULL),
	API_FSM_DEFINE_STATE(	NET_FSM_SV_WORK,
							NULL,
							net_osotb_fsm_sv_work_ongoing_handler,
							NULL),
	API_FSM_DEFINE_STATE(	NET_FSM_SV_SEND,
							NULL,
							net_osotb_fsm_sv_send_ongoing_handler,
							NULL),
};

/* Transitions of the fsm of this module: */
api_fsm_transition_t net_osotb_fsm_transitions_map[] =
{
	API_FSM_DEFINE_TRANSITION_MAP_ITEM(NET_FSM_SV_DISCONNECTED, NET_EV_SV_CONNECTED, NET_FSM_SV_CREATE),
	API_FSM_DEFINE_TRANSITION_MAP_ITEM(NET_FSM_SV_CREATE, NET_EV_SV_CREATED, NET_FSM_SV_WORK),
	API_FSM_DEFINE_TRANSITION_MAP_ITEM(NET_FSM_SV_WORK, NET_EV_SV_SEND_DATA_REQUEST, NET_FSM_SV_SEND),
	API_FSM_DEFINE_TRANSITION_MAP_ITEM(NET_FSM_SV_SEND, NET_EV_SV_DATA_TX_SUCCESS, NET_FSM_SV_WORK),
	API_FSM_DEFINE_TRANSITION_MAP_ITEM(NET_FSM_SV_SEND, NET_EV_SV_DATA_TX_TIMEOUT, NET_FSM_SV_WORK),
	API_FSM_DEFINE_TRANSITION_MAP_ITEM(NET_FSM_SV_SEND, NET_EV_SV_DISCONNECTED, NET_FSM_SV_DISCONNECTED),
	API_FSM_DEFINE_TRANSITION_MAP_ITEM(NET_FSM_SV_WORK, NET_EV_SV_DISCONNECTED, NET_FSM_SV_DISCONNECTED),
	API_FSM_DEFINE_TRANSITION_MAP_ITEM(NET_FSM_SV_CREATE, NET_EV_SV_DISCONNECTED, NET_FSM_SV_DISCONNECTED),
};

// Private variables definition -----------------------------------------------
/* Resources of the OSOTB server. */
#define NET_OSOTB_RES_UPTIME "UPTIME"

api_osotb_rx_handler_table_item_t rx_table[] =
{
	API_OSOTB_RX_HANDLER_TABLE_SET_ITEM(API_OSOTB_METHOD_GET, "uptime", net_osotb_rx_handler_get_uptime),
	API_OSOTB_RX_HANDLER_TABLE_SET_ITEM(API_OSOTB_METHOD_GET, "def_app_slot", net_osotb_rx_handler_get_def_app_slot),
	API_OSOTB_RX_HANDLER_TABLE_SET_ITEM(API_OSOTB_METHOD_PUT, "def_app_slot", net_osotb_rx_handler_put_def_app_slot),
	API_OSOTB_RX_HANDLER_TABLE_SET_ITEM(API_OSOTB_METHOD_GET, "op_mode", net_osotb_rx_handler_get_op_mode),
	API_OSOTB_RX_HANDLER_TABLE_SET_ITEM(API_OSOTB_METHOD_PUT, "op_mode", net_osotb_rx_handler_put_op_mode),
};

#define OSOTB_RX_SIZEOF_TABLE (sizeof(rx_table) / sizeof(rx_table[0]))

api_osotb_rx_t api_osotb_rx =
{
		.table = rx_table,
		.table_size = OSOTB_RX_SIZEOF_TABLE,
};

// Public functions definition  -----------------------------------------------
/*
 * @brief initalize the ftp server task.
 * @return RET_OK if success.
 * */
ret_code_t net_osotb_init(void)
{
	ret_code_t ret = RET_ERR;
	memset(&net_osotb, 0, sizeof(net_osotb));
	net_osotb.sv_id = NET_OSOTB_SERVER_ID;

	net_osotb.fsm.actual_state = &net_osotb_fsm_states[0];
	net_osotb.fsm.previous_state = &net_osotb_fsm_states[0];
	net_osotb.fsm.state_table = net_osotb_fsm_states;
	net_osotb.fsm.transition_map = net_osotb_fsm_transitions_map;
	net_osotb.fsm.states_qty = NET_FSM_MAX_STATES;
	net_osotb.fsm.transitions_qty = NET_MAX_EVENTS;

 	net_osotb.task.cmd_queue = xQueueCreate(NET_OSOTB_QUEUE_LEN, sizeof(net_msg_t));
	if( NULL != net_osotb.task.cmd_queue )
	{
		// TODO check if the queue was created
		if( xTaskCreate(	net_task_template, "net_osotb",
							NET_OSOTB_TASK_STACK_SIZE,
							&net_osotb,	/*!< Pass a pointer to the server struct to the task function to handle the server fsm. */
							NET_OSOTB_TASK_PRIOTY,
							&net_osotb.task.handle ) != pdPASS )
		{
			/* If the task can't be created the task handle will be NULL. */
			net_osotb.task.handle = NULL;
		}
		else
		{
			ret = RET_OK;
		}
	}
	return ret;
}

// Private functions definition -----------------------------------------------
/* Task Message handlers ----------------------------------------------------*/
/**
 * @brief  process NET_MSG_NET_DOWN_EVENT message.
 * @param msg queue message
 */
void net_osotb_process_msg_net_down_event(net_msg_t *msg)
{
	NET_OSOTB_LOG_WARNING("Device disconnected from the network.");
	/* Trigger the event that says to the FSM that the device is disconnected
	 * from the network. */
	api_fsm_process_event(&net_osotb.fsm, NET_EV_SV_DISCONNECTED, NULL);
}

/**
 * @brief  process NET_MSG_NET_UP_EVENT message.
 * @param msg queue message
 */
void net_osotb_process_msg_net_up_event(net_msg_t *msg)
{
	api_fsm_process_event(&net_osotb.fsm, NET_EV_SV_CONNECTED, NULL);
}

/**
 * @brief process a query of the status of the server.
 */
void net_osotb_process_msg_query_sv_status(net_msg_t *msg)
{
	if( NULL != msg )
	{
		if( (NET_FSM_SV_WORK == net_osotb.fsm.actual_state->id) ||
			(NET_FSM_SV_SEND == net_osotb.fsm.actual_state->id) )
		{
			app_msg_sv_status_t sv_status = {0};
			sv_status.sv_id = NET_OSOTB_SERVER_ID;
			sv_status.sv_up = true;
			app_send_msg_sv_status(sv_status);
		}
	}
	else
	{
		NET_OSOTB_LOG_ERROR("%s: msg is null!", __func__);
	}
}

/* FSM on_entry, ongoing and on_exit handlers -------------------------------*/
/*
 * @brief in this state, the task tries to create the server continuosly. And if
 * the server is created sucessfully, then go to the next state.
 * */
ret_code_t net_osotb_fsm_sv_create_ongoing_handler(void *arg)
{
	ret_code_t ret = RET_OK;
	ret = api_osotb_server_create(&api_osotb_rx);
	if( RET_OK == ret )
	{
		NET_OSOTB_LOG_INFO("%s: OSOTB server created!", __func__);
		api_fsm_process_event(&net_osotb.fsm, NET_EV_SV_CREATED, NULL);
	}
	return ret;
}

/*
 * @brief do an osotb server working cycle.
 * */
ret_code_t net_osotb_fsm_sv_work_ongoing_handler(void *arg)
{
	ret_code_t ret = RET_OK;
	ret = api_osotb_server_work();
	if( RET_OK == ret )
	{

	}
	return ret;
}

/*
 * @brief N/A
 * TODO this must be implemented.
 * */
ret_code_t net_osotb_fsm_sv_send_ongoing_handler(void *arg)
{
	ret_code_t ret = RET_OK;
	// TODO implement this
	return ret;
}

/* RX handlers of the OSOTB server --------------------------------------------*/
/**
 * @brief handler executed when a GET to the UPTIME resource is received.
 * @param payload in this case, the payload of the request is unused.
 * @param payload_response in this case, in the payload_response will be stored the
 * uptime of the devide.
 * @return RET_OK if success.
 */
ret_code_t net_osotb_rx_handler_get_uptime(char *payload, char *payload_response)
{
	if( NULL == payload_response ) return RET_ERR_NULL_POINTER;
	ret_code_t ret = RET_OK;
	(void) payload;
	char timestamp_str[API_DATETIME_TS_STR_LEN];
	API_Datetime_Get_Timestamp(timestamp_str, sizeof(API_DATETIME_TS_STR_LEN));
	sprintf(payload_response, "%s(%lu)", timestamp_str, TICKS_TO_MS(xTaskGetTickCount()));
	return ret;
}

/**
 * @brief handler executed when a GET to the def_app_slot resource is received.
 * @param payload in this case, the payload of the request is unused.
 * @param payload_response in this case, in the payload_response will store the
 * default app_slot of the device.
 * @return RET_OK if success.
 */
ret_code_t net_osotb_rx_handler_get_def_app_slot(char *payload, char *payload_response)
{
	if( NULL == payload_response ) return RET_ERR_NULL_POINTER;
	ret_code_t ret = RET_OK;
	(void) payload;
	api_iap_app_slot_t def_app_slot = api_iap_get_default_app();
	sprintf(payload_response, "%lu", (uint32_t) def_app_slot);
	return ret;
}

/**
 * @brief handler executed when a PUT to the def_app_slot resource is received.
 * @param payload in this case, the payload of request will have the default app slot to set.
 * @param payload_response in this case, this value is unused.
 * @return RET_OK if success.
 */
ret_code_t net_osotb_rx_handler_put_def_app_slot(char *payload, char *payload_response)
{
	if( NULL == payload_response ) return RET_ERR_NULL_POINTER;
	ret_code_t ret = RET_OK;
	(void) payload;
	api_iap_app_slot_t app_slot = (api_iap_app_slot_t) atoi(payload);
	ret = api_iap_set_default_app(app_slot);
	if( RET_OK != ret )
	{
		NET_OSOTB_LOG_ERROR("%s: app_slot number not recognized! (app_slot %d)", __func__, app_slot);
	}
	else
	{
		NET_OSOTB_LOG_INFO("%s: app_slot flashed correctly! (app_slot %d)", __func__, app_slot);
	}
	return ret;
}

/**
 * @brief handler executed when a GET to the op_mode resource is received.
 * @param payload in this case, the payload of the request is unused.
 * @param payload_response in this case, in the payload_response will store the
 * operation mode of the device.
 * @return RET_OK if success.
 */
ret_code_t net_osotb_rx_handler_get_op_mode(char *payload, char *payload_response)
{
	if( NULL == payload_response ) return RET_ERR_NULL_POINTER;
	ret_code_t ret = RET_OK;
	(void) payload;
	ret = app_get_op_mode_string(payload_response);
	return ret;
}

/**
 * @brief handler executed when a PUT to the def_app_slot resource is received.
 * @param payload in this case, the payload of request will have the default app slot to set.
 * @param payload_response in this case, this value is unused.
 * @return RET_OK if success.
 */
ret_code_t net_osotb_rx_handler_put_op_mode(char *payload, char *payload_response)
{
	if( NULL == payload_response ) return RET_ERR_NULL_POINTER;
	ret_code_t ret = RET_OK;
	(void) payload;
	if( false == app_represents_valid_op_mode(payload) ) ret = RET_ERR_INVALID_PARAMS;
	else ret = app_send_msg_change_op_mode_cmd(payload);
	return ret;
}

