/*
 * sec_exp_msg.c
 *
 *  Created on: Jun 6, 2021
 *      Author: Julian Rodriguez aka Marifante 
 *		Email: jnrodriguezz@hotmail.com
 *		Gitlab: https://gitlab.com/Marifante
 *		Github: https://github.com/Marifante
 */

// Include --------------------------------------------------------------------
#include "sec_exp_private.h" 	/* !< App module private include. */

// Private variables declaration ----------------------------------------------
static ret_code_t sec_exp_send_msg(sec_exp_msg_t *msg);
/* App Task Messages handlers prototypes: */
static void sec_exp_process_msg_start(sec_exp_msg_t *msg);
static void sec_exp_process_msg_halt(sec_exp_msg_t *msg);

// Boot App Task message-handler table ----------------------------------------
/* App Task Messages handlers table: */
#define SEC_EXP_PROCESS_MSG(x,y) {.id = x, .handler=y}
static const sec_exp_process_msg_table_t sec_exp_process_msg_table[] =
{
	SEC_EXP_PROCESS_MSG(SEC_EXP_MSG_START, sec_exp_process_msg_start),
	SEC_EXP_PROCESS_MSG(SEC_EXP_MSG_HALT, sec_exp_process_msg_halt),
};
#define SIZEOF_SEC_EXP_PROCESS_MSG_TABLE (sizeof(sec_exp_process_msg_table) / sizeof(sec_exp_process_msg_table[0]))

// Public functions definition  -----------------------------------------------
/*
 * @brief used by another task to send to the sec exp task that must to
 * start the experiment.
 * */
ret_code_t sec_exp_send_msg_start(sec_exp_start_parameters_t start_params)
{
	sec_exp_msg_t msg;
	msg.data.start_params.sampling_freq = start_params.sampling_freq;
	msg.data.start_params.temp_nodes_mask = start_params.temp_nodes_mask;
	msg.id = SEC_EXP_MSG_START;
	return sec_exp_send_msg(&msg);
}

/*
 * @brief used by another task to send to the sec_exp task that must to
 * stop the experiment.
 * */
ret_code_t sec_exp_send_msg_halt(void)
{
	sec_exp_msg_t msg;
	msg.id = SEC_EXP_MSG_HALT;
	return sec_exp_send_msg(&msg);
}

/**
 * @brief send a message to the sec_exp task through its queue.
 * This function can be called from an ISR too.
 * */
static ret_code_t sec_exp_send_msg(sec_exp_msg_t *msg)
{
	BaseType_t rtos_result = pdTRUE;
	ret_code_t res = RET_OK;

	if( (NULL == sec_exp.cmd_queue) || (NULL == msg) )
	{
		return RET_ERR;
	}
	else
	{
		if(pdTRUE != xPortIsInsideInterrupt())
		{
			/* If the core isn't in an interrupt context then send the msg normally. */
			rtos_result = xQueueSend( sec_exp.cmd_queue, msg, ( TickType_t ) 10 );
		}
		else
		{
				/* If the core is in an interrupt context then use send from ISR API. */
			portBASE_TYPE xHigherPriorityTaskWoken;
			BaseType_t yield_req = pdFALSE;

			rtos_result = xQueueSendFromISR( sec_exp.cmd_queue, msg, &xHigherPriorityTaskWoken );
			// Now the buffer is empty we can switch context if necessary.
			if( xHigherPriorityTaskWoken )
			{
			  // Actual macro used here is port specific.
			  portYIELD_FROM_ISR(yield_req);
			}
		}
		res = (pdTRUE == rtos_result) ? RET_OK : RET_ERR;
		return res;
	}
}

// Private functions definition -----------------------------------------------
/**
 *
 * @brief  process incoming queue messages. This function calls the handler
 * associated with that message.
 *
 * @param msg queue message
 *
 */
void sec_exp_process_msg(sec_exp_msg_t *msg)
{
	uint8_t c;
	if( NULL != msg )
	{
		for(c = 0; c < SIZEOF_SEC_EXP_PROCESS_MSG_TABLE; c++)
		{
			if(sec_exp_process_msg_table[c].id == msg->id)
			{
				// found, check callback
				if(NULL != sec_exp_process_msg_table[c].handler)
				{
					// call it
					sec_exp_process_msg_table[c].handler(msg);
				}
				// finish search
				break;
			}
		}
	}
}

/**
 * @brief another task advised to sec exp task that must to start the experiment!
 * */
static void sec_exp_process_msg_start(sec_exp_msg_t *msg)
{
	if( NULL != msg )
	{
		SEC_EXP_LOG_INFO("%s: starting experiment!", __func__);
		api_fsm_process_event(&sec_exp.fsm, SEC_EXP_EV_START_CMD_RECEIVED, NULL); // TODO here we can pass arguments to the experiment!
	}
	else
	{
		SEC_EXP_LOG_ERROR("%s: msg is null!", __func__);
	}
}

/**
 * @brief another task advised to sec exp task that must to halt the experiment!
 * */
static void sec_exp_process_msg_halt(sec_exp_msg_t *msg)
{
	if( NULL != msg )
	{
		SEC_EXP_LOG_INFO("%s: halting experiment!", __func__);
		api_fsm_process_event(&sec_exp.fsm, SEC_EXP_EV_HALT_CMD_RECEIVED, NULL); // TODO here we can pass arguments to the experiment!
	}
	else
	{
		SEC_EXP_LOG_ERROR("%s: msg is null!", __func__);
	}
}
