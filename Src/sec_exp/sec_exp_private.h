/*
 * sec_exp_private.h
 *
 *  Created on: Jun 6, 2021
 *      Author: Julian Rodriguez aka Marifante 
 *		Email: jnrodriguezz@hotmail.com
 *		Gitlab: https://gitlab.com/Marifante
 *		Github: https://github.com/Marifante
 */

#ifndef SEC_EXP_PRIVATE_H_
#define SEC_EXP_PRIVATE_H_

// Includes -------------------------------------------------------------------
/* Tasks modules includes. */
#include "sec_exp.h"
#include "app.h"

#include "api.h"	/* !< OBCs API include. */

// Macros and constants -------------------------------------------------------
#define SEC_EXP_LOG_ENABLED			1		/*<! Enables module log. */

#define	LOG_TAG						"sec_exp"	/*<! Tag assigned to logs for this module. */

#if SEC_EXP_LOG_ENABLED
	#define SEC_EXP_LOG_ERROR(format, ...)		LOG_ERROR(LOG_TAG, format, ##__VA_ARGS__)
	#define SEC_EXP_LOG_WARNING(format, ...)	LOG_WARNING(LOG_TAG, format, ##__VA_ARGS__)
	#define SEC_EXP_LOG_INFO(format, ...)		LOG_INFO(LOG_TAG, format, ##__VA_ARGS__)
	#define SEC_EXP_LOG_DEBUG(format, ...)		LOG_DEBUG(LOG_TAG, format, ##__VA_ARGS__)
#else
	#define SEC_EXP_LOG_ERROR(...)
	#define SEC_EXP_LOG_WARNING(...)
	#define SEC_EXP_LOG_INFO(...)
	#define SEC_EXP_LOG_DEBUG(...)
#endif

// Private variables declaration ----------------------------------------------

typedef struct
{
	TaskHandle_t task_handle;
	QueueHandle_t cmd_queue;  	/* !< Other tasks or ISR will notify sec_exp task about some events with this queue. */
	/* This struct have the extended vars of the states of the fsm. With this vars, certain
	 * states of the fsm will do some operations to evaluate the flow of the operation. */
	struct
	{
		struct
		{
			char file_path[80]; /*!< The name of the file of the report. */
			API_FS_FILE *file; /*!< File where the results of the experiment will be stored. */
			int entry_time; /*!< To take in count the time from the most recent entry in the state. */
			int last_measure_time; /*!< The time of the last measure. */
		}measure;
	}fsm_extended_vars;
	api_fsm_t fsm;	/* !< Struct that holds the information of fsm of this module. */
} sec_exp_t;

typedef struct
{
  uint8_t id;
  void (*handler)(sec_exp_msg_t *msg);
}sec_exp_process_msg_table_t;

// Private variables definition -----------------------------------------------
sec_exp_t sec_exp;

// Private functions declaration ----------------------------------------------
void sec_exp_process_msg(sec_exp_msg_t *msg);

// Module finite state machine declarations -----------------------------------
typedef enum
{
	SEC_EXP_STATE_IDLE = 0,
	SEC_EXP_STATE_MEASURE,
	SEC_EXP_MAX_STATES,	/*!< Max states. */
}sec_exp_fsm_state_id_t;

/*
 * @brief events that will trigger transitions in this fsm:
 * */
typedef enum
{
	SEC_EXP_EV_START_CMD_RECEIVED = 0,		/*!< The app task sends a message saying we need to start the experiment! */
	SEC_EXP_EV_HALT_CMD_RECEIVED,	/*!< The app task sends a message saying we need to stop the experiment! */
	SEC_EXP_EV_MAX_TIME_REACHED,	/*!< The experiment reached its maximum time! */
	SEC_EXP_MAX_EVENTS			/*!< Max events on the fsm. */
}sec_exp_fsm_state_events_t;

/* SEC_EXP_STATE_MEASURE */
#define SEC_EXP_MEASURE_SAMPLING_PERIOD_MS	10000	/*!< The exp will measure the temp every this period. */
#define SEC_EXP_MEASURE_DEFAULT_EXP_TIMEOUT_MS 60000 /* !< Timeout for the experiment! */
#define SEC_EXP_REPORT_LINE_MAX_LEN 256	/* !< Max length of each line of the report. */
/* The format of each line of the report file. */
ret_code_t sec_exp_fsm_measure_on_entry_handler(void *arg);
ret_code_t sec_exp_fsm_measure_ongoing_handler(void *arg);
ret_code_t sec_exp_fsm_measure_on_exit_handler(void);

#endif /* SEC_EXP_PRIVATE_H_ */
