/*
 * sec_exp.c
 *
 *  Created on: March 15, 2021
 *      Authors:
 *      Lucas Mancini
 *      Julian Rodriguez aka marifante
 */

// Include --------------------------------------------------------------------
#include "sec_exp_private.h" 	/* !< App module private include. */

// Macros & constants ---------------------------------------------------------
#define SEC_EXP_TASK_DELAY_MS			50			/*<! in ms, delay of the task. */
#define SEC_EXP_TASK_STACK_SIZE			1024
#define SEC_EXP_TASK_PRIOTY				osPriorityNormal
#define SEC_EXP_QUEUE_TIMEOUT			(10 / portTICK_PERIOD_MS)	// See if we can lower this
#define SEC_EXP_QUEUE_LEN				10

// Private functions declaration ----------------------------------------------
void sec_exp_task(void *arg);

// Private variables definition -----------------------------------------------
/* States of the fsm of this module: */
api_fsm_state_t sec_exp_fsm_states[] =
{
	API_FSM_DEFINE_STATE(	SEC_EXP_STATE_IDLE,
							NULL,
							NULL,
							NULL),
	API_FSM_DEFINE_STATE(	SEC_EXP_STATE_MEASURE,
							sec_exp_fsm_measure_on_entry_handler,
							sec_exp_fsm_measure_ongoing_handler,
							sec_exp_fsm_measure_on_exit_handler),
};

/* Transitions of the fsm of this module: */
api_fsm_transition_t sec_exp_fsm_transitions_map[] =
{
	API_FSM_DEFINE_TRANSITION_MAP_ITEM(SEC_EXP_STATE_IDLE, SEC_EXP_EV_START_CMD_RECEIVED, SEC_EXP_STATE_MEASURE),
	API_FSM_DEFINE_TRANSITION_MAP_ITEM(SEC_EXP_STATE_MEASURE, SEC_EXP_EV_HALT_CMD_RECEIVED, SEC_EXP_STATE_IDLE),
	API_FSM_DEFINE_TRANSITION_MAP_ITEM(SEC_EXP_STATE_MEASURE, SEC_EXP_EV_MAX_TIME_REACHED, SEC_EXP_STATE_IDLE),
};

// Public functions definition  -----------------------------------------------
/**
 * @brief init sec_exp module of bootloader.
 * @return RET_OK if success.
 * */
TaskHandle_t sec_exp_init(void *arg)
{
	/* Init fsm struct: */
	sec_exp.fsm.actual_state = &sec_exp_fsm_states[0];
	sec_exp.fsm.previous_state = &sec_exp_fsm_states[0];
	sec_exp.fsm.state_table = sec_exp_fsm_states;
	sec_exp.fsm.transition_map = sec_exp_fsm_transitions_map;
	sec_exp.fsm.states_qty = SEC_EXP_MAX_STATES;
	sec_exp.fsm.transitions_qty = SEC_EXP_MAX_EVENTS;

	/* Create commands queue and task: */
	sec_exp.cmd_queue = xQueueCreate(SEC_EXP_QUEUE_LEN, sizeof(sec_exp_msg_t));
	if( NULL != sec_exp.cmd_queue )
	{
		if( xTaskCreate(	sec_exp_task, "sec_exp",
							SEC_EXP_TASK_STACK_SIZE,
							NULL,
							SEC_EXP_TASK_PRIOTY,
							&sec_exp.task_handle ) != pdPASS )
		{
			/* If the task can't be created the task handle will be NULL. */
			sec_exp.task_handle = NULL;
		}
	}
	return sec_exp.task_handle;
}

// Private functions definition -----------------------------------------------
void sec_exp_task(void *arg)
{
	sec_exp_msg_t msg;

	while(true)
	{
		if(pdTRUE == xQueueReceive(sec_exp.cmd_queue, (uint8_t*)&msg, SEC_EXP_QUEUE_TIMEOUT))
		{
			sec_exp_process_msg(&msg); // process incoming message
		}
		api_fsm_work(&sec_exp.fsm, NULL);
	}
}

/* Definition of the handlers of the fsm: */
/* SEC_EXP_FSM_STATE_MEASURE functions ----------------------------------------*/
/*
 * @brief sends a message to the experiment tasks saying them that they need to
 * start to execute the experiments!
 * */
ret_code_t sec_exp_fsm_measure_on_entry_handler(void *arg)
{
	ret_code_t ret = RET_OK;
	sec_exp.fsm_extended_vars.measure.entry_time = TICKS_TO_MS(xTaskGetTickCount());
	sec_exp.fsm_extended_vars.measure.last_measure_time = sec_exp.fsm_extended_vars.measure.entry_time;
	sec_exp.fsm_extended_vars.measure.file = NULL;
	SEC_EXP_LOG_INFO("%s: starting the measurements! (%d)",__func__, sec_exp.fsm_extended_vars.measure.entry_time);

	/* Try to create the directory where the reports will be stored. If they are created already,
	 * the mkdir function will return a success value! */
	if( 0 == API_FS_mkdir(SEC_EXP_REPORTS_FOLDER) )
	{
		char timestamp_str[API_DATETIME_TS_STR_LEN];
		ret = API_Datetime_Get_Timestamp_Basic(timestamp_str, sizeof(timestamp_str));
		{
			sprintf(sec_exp.fsm_extended_vars.measure.file_path, "%s/e2_%s", SEC_EXP_REPORTS_FOLDER, timestamp_str);
			sec_exp.fsm_extended_vars.measure.file = API_FS_fopen(sec_exp.fsm_extended_vars.measure.file_path, "w");
			if( NULL == sec_exp.fsm_extended_vars.measure.file )
			{
				SEC_EXP_LOG_ERROR("%s: can't create report file %s, experiment will not be executed...",__func__, sec_exp.fsm_extended_vars.measure.file_path);
			}
			else
			{
				ret = api_temp_sensor_init();
				if( RET_OK != ret )
				{
					SEC_EXP_LOG_ERROR("%s: error configuring the temp sensors!", __func__);
				}
			}
		}
	}
	else
	{
		SEC_EXP_LOG_INFO("%s: %s is not created and can't be created!",__func__, SEC_EXP_REPORTS_FOLDER);
	}

	return ret;
}

/*
 * @brief if the timeout has been reached, retry the init process. An event
 * triggered from the net task will send this fsm to the idle state.
 * */
ret_code_t sec_exp_fsm_measure_ongoing_handler(void *arg)
{
	ret_code_t ret = RET_OK;
	int now = TICKS_TO_MS(xTaskGetTickCount());
	if( SEC_EXP_MEASURE_DEFAULT_EXP_TIMEOUT_MS < (now - sec_exp.fsm_extended_vars.measure.entry_time) )
	{
		ret = api_fsm_process_event(&sec_exp.fsm, SEC_EXP_EV_MAX_TIME_REACHED, NULL);
	}

	if( NULL != sec_exp.fsm_extended_vars.measure.file )
	{
		if( SEC_EXP_MEASURE_SAMPLING_PERIOD_MS < (now - sec_exp.fsm_extended_vars.measure.last_measure_time) )
		{
			char report_line[SEC_EXP_REPORT_LINE_MAX_LEN];
			SEC_EXP_LOG_INFO("%s: trying to measure temp! (%d < %d)!", __func__, SEC_EXP_MEASURE_SAMPLING_PERIOD_MS, (now - sec_exp.fsm_extended_vars.measure.last_measure_time));
			ret = api_temp_sensor_measure(report_line);
			SEC_EXP_LOG_INFO("%s: %s", __func__, report_line);
			uint32_t bytes_written = API_FS_fwrite(report_line, 1, strlen(report_line), sec_exp.fsm_extended_vars.measure.file);
			if( strlen(report_line) != bytes_written )
			{
				SEC_EXP_LOG_ERROR("%s: error writing values on the report file! (bw %d != strlen %d)", __func__, bytes_written, strlen(report_line));
			}

			sec_exp.fsm_extended_vars.measure.last_measure_time = TICKS_TO_MS(xTaskGetTickCount());
		}
	}
	return ret;
}

/*
 * @brief the on_exit handler of the measure state will close the file.
 * */
ret_code_t sec_exp_fsm_measure_on_exit_handler(void)
{
	ret_code_t ret = RET_OK;
	SEC_EXP_LOG_INFO("%s: ending experiment!", __func__);

	if( NULL != sec_exp.fsm_extended_vars.measure.file )
	{
		if( 0 != API_FS_fclose(sec_exp.fsm_extended_vars.measure.file))
		{
			SEC_EXP_LOG_ERROR("%s: can't close report file %s",__func__, sec_exp.fsm_extended_vars.measure.file_path);
		}
	}

	return ret;
}
