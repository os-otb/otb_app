/*
 * sec_exp_types.h
 *
 *  Created on: Aug 19, 2021
 *      Author: marifante
 */

#ifndef SEC_EXP_TYPES_H_
#define SEC_EXP_TYPES_H_

// Includes -------------------------------------------------------------------
#include "api_types.h"

// Macros & Constants ---------------------------------------------------------
/* For now, the secondary experiment task will store the reports in a folder
 * called exp2 that is in the root of the FTP server filesystem. */
#define SEC_EXP_REPORTS_DISK_PATH	API_FTP_SERVER_ROOT_DIR_DISK_PATH
#define SEC_EXP_REPORTS_FOLDER		API_FTP_SERVER_ROOT_DIR "/exp2" /*!< Folder where the repor*/

// Public variable declaration ------------------------------------------------
typedef enum
{
  SEC_EXP_MSG_DEFAULT = 0,
  SEC_EXP_MSG_START,	/* !< If the task receives this message, starts the experiment. */
  SEC_EXP_MSG_HALT,	/* !< If the task receives this message, if it is doing the experiment, will stop it. */
}sec_exp_msg_id_t;

/* Parameters of the experiment sent by the app task! */
typedef struct
{
	uint32_t temp_nodes_mask; /* !< Mask that will hold the temperature nodes to measure. */
	uint32_t sampling_freq; /* !< Mask that will hold sampling frequency of the temp nodes. */
}sec_exp_start_parameters_t;

/**
 * @brief message packet format sent to the sec_exp task.
 * The data field is an union with the different data packets that can be sent
 * to the task.
 */
typedef struct //STRUCT_PACKET
{
	sec_exp_msg_id_t id;
	union
	{
		sec_exp_start_parameters_t start_params;
	}data;
}sec_exp_msg_t;


#endif /* SEC_EXP_TYPES_H_ */
