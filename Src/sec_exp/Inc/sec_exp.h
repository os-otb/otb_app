/*
 * sec_exp.h
 *
 *  Created on: May 19, 2021
 *      Author: marifante
 */

#ifndef TIAB_EXP_H_
#define TIAB_EXP_H_

// Include --------------------------------------------------------------------
#include "sec_exp_types.h"
#include "api_types.h"
#include "rtos.h"

// Bootloader modules types

// Public functions declaration  ----------------------------------------------
TaskHandle_t sec_exp_init(void *arg);

/* Functions used by other tasks to send msgs to the sec_exp task. */
ret_code_t sec_exp_send_msg_start(sec_exp_start_parameters_t start_params); /*!< Used by another task to start the secondary experiment. */
ret_code_t sec_exp_send_msg_halt(void); /*!< Used by another task to halt the secondary experiment. */

#endif /* TIAB_EXP_H_ */
