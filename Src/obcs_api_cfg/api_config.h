/*
 * api_config.h
 *
 *  Created on: Jul 18, 2021
 *      Author: marifante
 */

#ifndef API_CONFIG_H_
#define API_CONFIG_H_

#include "project.h"

#ifdef OTB
#include "api_config_otb.h"
#endif

/* API default config. This must be here to fill the configurations that has been left. */
#include "api_config_default.h"

#endif /* API_CONFIG_H_ */
