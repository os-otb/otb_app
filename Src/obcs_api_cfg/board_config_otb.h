/*
 * board_config_otb.c
 *
 *  Created on: Aug 5, 2021
 *      Author: julian alias Marifante
 *		Email: jnrodriguezz@hotmail.com
 *		Gitlab: https://gitlab.com/Marifante
 *		Github: https://github.com/Marifante
 */

#ifndef BOARD_CONFIG_OTB_C_
#define BOARD_CONFIG_OTB_C_

// Includes -------------------------------------------------------------------
#include "board_cfg_types.h"

// MCU ------------------------------------------------------------------------
#define STM32F407Z	/*!< The MCU used. */

// GPIOs ----------------------------------------------------------------------
/* @brief number of GPIO used for digital outputs and inputs only and its purpose.*/
typedef enum
{
	API_GPIO_DBG_LED0 = 0,
	API_GPIO_DBG_LED1,
	API_GPIO_COND1,
	API_GPIO_COND2,
	API_GPIO_COND3,
	API_GPIO_MAX		/*<! Number of GPIOs used. */
}API_GPIO_Port_Id_T;

#define BOARD_GPIO_SIZEOF_TABLE API_GPIO_MAX /*!< The number of elements in the board_gpio_handler_table.*/

#define BOARD_GPIO_DEBUG_LED0_GPORT		GPIOC
#define BOARD_GPIO_DEBUG_LED0_GPIN		GPIO_PIN_13
#define BOARD_GPIO_DEBUG_LED1_GPORT		GPIOC
#define BOARD_GPIO_DEBUG_LED1_GPIN		GPIO_PIN_13
#define BOARD_GPIO_COND1_GPORT			GPIOC
#define BOARD_GPIO_COND1_GPIN			GPIO_PIN_6
#define BOARD_GPIO_COND2_GPORT			GPIOG
#define BOARD_GPIO_COND2_GPIN			GPIO_PIN_7
#define BOARD_GPIO_COND3_GPORT			GPIOG
#define BOARD_GPIO_COND3_GPIN			GPIO_PIN_8

// USARTs ---------------------------------------------------------------------
/* @brief number of USARTs used and its purpose. */
typedef enum
{
	API_USART_SERIAL_LOG = 0,	/*<! USART port used by the serial logger. */
	API_USART_MAX_PORTS			/*<! Number of USART ports used in the API. */
}API_Usart_Port_Id_T;

#define BOARD_USART_SIZEOF_TABLE API_USART_MAX_PORTS

#define BOARD_USART_SERIAL_LOG			USART3			/*!< USART used to send the serial logs by the board. Must be an USART instance. */
#define BOARD_USART_SERIAL_LOG_APB		LL_APB1_GRP1_PERIPH_USART3
#define BOARD_USART_SERIAL_LOG_IRQN		USART3_IRQn		/*!< Interrupt Request number of the interrupt of the USART used.*/
#define BOARD_USART_SERIAL_LOG_TX_GPORT	GPIOD
#define BOARD_USART_SERIAL_LOG_TX_GPIN	LL_GPIO_PIN_8	/*!< GPIO Pin used by USART TX. Must to be the LL driver of STM32Cube HAL definition. */
#define BOARD_USART_SERIAL_LOG_TX_AHB	LL_AHB1_GRP1_PERIPH_GPIOD
#define BOARD_USART_SERIAL_LOG_TX_AF	LL_GPIO_AF_7
#define BOARD_USART_SERIAL_LOG_RX_GPORT	GPIOD
#define BOARD_USART_SERIAL_LOG_RX_GPIN	LL_GPIO_PIN_9	/*!< GPIO Pin used by USART RX. Must to be the LL driver of STM32Cube HAL definition. */
#define BOARD_USART_SERIAL_LOG_RX_AHB	LL_AHB1_GRP1_PERIPH_GPIOD
#define BOARD_USART_SERIAL_LOG_RX_AF	LL_GPIO_AF_7

// Ethernet -------------------------------------------------------------------
#define BOARD_ETH_RMII_CLK_GPORT		GPIOA			/*!< Checked! */
#define BOARD_ETH_RMII_CLK_GPIN			GPIO_PIN_1		/*!< Checked! */
#define BOARD_ETH_RMII_MDIO_GPORT		GPIOA			/*!< Checked! */
#define BOARD_ETH_RMII_MDIO_GPIN		GPIO_PIN_2		/*!< Checked! */
#define BOARD_ETH_RMII_MDC_GPORT		GPIOC			/*!< Checked! */
#define BOARD_ETH_RMII_MDC_GPIN			GPIO_PIN_1		/*!< Checked! */
#define BOARD_ETH_RMII_CRSDV_GPORT		GPIOA			/*!< Checked! */
#define BOARD_ETH_RMII_CRSDV_GPIN		GPIO_PIN_7		/*!< Checked! */
#define BOARD_ETH_RMII_RX0_GPORT		GPIOC			/*!< Checked! */
#define BOARD_ETH_RMII_RX0_GPIN			GPIO_PIN_4		/*!< Checked! */
#define BOARD_ETH_RMII_RX1_GPORT		GPIOC			/*!< Checked! */
#define BOARD_ETH_RMII_RX1_GPIN			GPIO_PIN_5		/*!< Checked! */
#define BOARD_ETH_RMII_TX0_GPORT		GPIOG			/*!< Checked! */
#define BOARD_ETH_RMII_TX0_GPIN			GPIO_PIN_13		/*!< Checked! */
#define BOARD_ETH_RMII_TX1_GPORT		GPIOG			/*!< Checked! */
#define BOARD_ETH_RMII_TX1_GPIN			GPIO_PIN_14		/*!< Checked! */
#define BOARD_ETH_RMII_TXEN_GPORT		GPIOB			/*!< Checked! */
#define BOARD_ETH_RMII_TXEN_GPIN		GPIO_PIN_11		/*!< Checked! */
#define BOARD_ETH_RMII_RXERR_GPORT		GPIOE
#define BOARD_ETH_RMII_RXERR_GPIN		GPIO_PIN_15
#define BOARD_ETH_RST_GPORT				GPIOB
#define BOARD_ETH_RST_GPIN				GPIO_PIN_15
#define BOARD_ETH_INT_GPORT				GPIOE
#define BOARD_ETH_INT_GPIN				GPIO_PIN_14

// MB85RS SPI & Timers configuration ----------------------------------------
/* MB85RS SPI Chip Select. */
#define BOARD_MB85RS_CS_GPORT			GPIOD
#define BOARD_MB85RS_CS_GPIN			GPIO_PIN_3	/* Used by HAL driver functions. */
#define BOARD_MB85RS_CS_GPIN_NUMBER		3			/* The number of the GPIO (used by diskio driver to make some things). */

/* MB85RS MISO, MOSI & CLK. */
#define BOARD_MB85RS_DISKIO_MISO_GPORT	GPIOC
#define BOARD_MB85RS_DISKIO_MISO_GPIN	GPIO_PIN_2
#define BOARD_MB85RS_DISKIO_MOSI_GPORT	GPIOC
#define BOARD_MB85RS_DISKIO_MOSI_GPIN	GPIO_PIN_3
#define BOARD_MB85RS_DISKIO_CLK_GPORT	GPIOB
#define BOARD_MB85RS_DISKIO_CLK_GPIN	GPIO_PIN_10

#define BOARD_MB85RS_DISKIO_SPI_INSTANCE			SPI2	/*!< SPI used to handle the MB85RS card. */
#define BOARD_MB85RS_DISKIO_GPIO_AF					GPIO_AF5_SPI2
#define BOARD_MB85RS_DISKIO_SPI_CLK_ENABLE() 		__HAL_RCC_SPI2_CLK_ENABLE()
#define BOARD_MB85RS_DISKIO_CS_GPIO_CLK_ENABLE()	__HAL_RCC_GPIOD_CLK_ENABLE()
#define BOARD_MB85RS_DISKIO_MISO_GPIO_CLK_ENABLE()	__HAL_RCC_GPIOC_CLK_ENABLE()
#define BOARD_MB85RS_DISKIO_MOSI_GPIO_CLK_ENABLE()	__HAL_RCC_GPIOC_CLK_ENABLE()
#define BOARD_MB85RS_DISKIO_CLK_GPIO_CLK_ENABLE()	__HAL_RCC_GPIOB_CLK_ENABLE()

//#ifdef API_FS_USE_SD
#if 0
// Micro SD SPI & Timers configuration ----------------------------------------
/* Micro SD SPI Chip Select. */
#define BOARD_SD_CS_GPORT			GPIOC
#define BOARD_SD_CS_GPIN			GPIO_PIN_11	/* Used by HAL driver functions. */
#define BOARD_SD_CS_GPIN_NUMBER		11			/* The number of the GPIO (used by diskio driver to make some things). */
#define BOARD_SD_DISKIO_CS_GPIO_CLK_ENABLE()	__HAL_RCC_GPIOC_CLK_ENABLE()

/* Micro SD SPI MISO, MOSI & CLK. */
#define BOARD_SD_DISKIO_MISO_GPORT	GPIOC
#define BOARD_SD_DISKIO_MISO_GPIN	GPIO_PIN_8
#define BOARD_SD_DISKIO_MISO_GPIO_CLK_ENABLE()	__HAL_RCC_GPIOC_CLK_ENABLE()

#define BOARD_SD_DISKIO_MOSI_GPORT	GPIOD
#define BOARD_SD_DISKIO_MOSI_GPIN	GPIO_PIN_2
#define BOARD_SD_DISKIO_MOSI_GPIO_CLK_ENABLE()	__HAL_RCC_GPIOD_CLK_ENABLE()

#define BOARD_SD_DISKIO_CLK_GPORT	GPIOC
#define BOARD_SD_DISKIO_CLK_GPIN	GPIO_PIN_12
#define BOARD_SD_DISKIO_CLK_GPIO_CLK_ENABLE()	__HAL_RCC_GPIOC_CLK_ENABLE()

#define BOARD_SD_DISKIO_SPI_INSTANCE			SPI3	/*!< SPI used to handle the SD card. */
#define BOARD_SD_DISKIO_GPIO_AF					GPIO_AF6_SPI3
#define BOARD_SD_DISKIO_SPI_CLK_ENABLE() 		__HAL_RCC_SPI3_CLK_ENABLE()

/* SD Diskio main timer selection: */
#define BOARD_SD_DISKIO_MAIN_TIM					TIM2
#define BOARD_SD_DISKIO_MAIN_TIM_APB				APB1
#define BOARD_SD_DISKIO_MAIN_TIM_APB1_GRP1			LL_APB1_GRP1_PERIPH_TIM2
#define BOARD_SD_DISKIO_MAIN_TIM_CLK_SOURCE			LL_TIM_CLOCKSOURCE_INTERNAL
#define BOARD_SD_DISKIO_MAIN_TIM_IRQn				TIM2_IRQn
#define Board_SD_Diskio_Main_TIM_IRQHandler			TIM2_IRQHandler /* !< Re-definition of the ISR handler. Is implemented in the driver itself. */

/* SD Diskio second timer selection: */
#define BOARD_SD_DISKIO_SECONDARY_TIM				TIM5
#define BOARD_SD_DISKIO_SECONDARY_TIM_APB			APB1
#define BOARD_SD_DISKIO_SECONDARY_TIM_APB1_GRP1		LL_APB1_GRP1_PERIPH_TIM5
#define BOARD_SD_DISKIO_SECONDARY_TIM_CLK_SOURCE	LL_TIM_CLOCKSOURCE_INTERNAL
#define BOARD_SD_DISKIO_SECONDARY_TIM_IRQn			TIM5_IRQn
#define Board_SD_Diskio_Secondary_TIM_IRQHandler	TIM5_IRQHandler /* !< Re-definition of the ISR handler. Is implemented in the driver itself. */
#endif

// ADC ------------------------------------------------------------------------

/* @brief sensors attached to the ADC1. */
typedef enum
{
	BOARD_ADC1_ANALOG_INPUT_LMT85_TS0 = 0,
	BOARD_ADC1_ANALOG_INPUT_LMT85_TS1,
	BOARD_ADC1_ANALOG_INPUT_LMT85_TS2,
	BOARD_ADC1_ANALOG_INPUT_LMT85_TS3,
	BOARD_ADC1_MAX_ANALOG_INPUTS		/* !< Max number of analog inputs on the board measured iwth ADC1. */
}board_adc1_analog_input_t;

/* @brief sensors attached to the ADC3. */
typedef enum
{
	BOARD_ADC3_ANALOG_INPUT_LMT85_TN0,
	BOARD_ADC3_ANALOG_INPUT_LMT85_TN1,
	BOARD_ADC3_ANALOG_INPUT_LMT85_TN2,
	BOARD_ADC3_ANALOG_INPUT_LMT85_TN3,
	BOARD_ADC3_ANALOG_INPUT_LMT85_TN4,
	BOARD_ADC3_ANALOG_INPUT_LMT85_TN5,
	BOARD_ADC3_MAX_ANALOG_INPUTS		/* !< Max number of analog inputs on the board measure with ADC3. */
}board_adc3_analog_input_t;

// LMT85 sensors --------------------------------------------------------------
#define BOARD_USE_LMT85 /*!< Enable the module! */

/* LMT85 driver sampling timer: */
#define BOARD_LMT85_SAMPLING_TIM					TIM12
#define BOARD_LMT85_SAMPLING_TIM_APB				APB1
#define BOARD_LMT85_SAMPLING_TIM_APB_GRP			LL_APB1_GRP1_PERIPH_TIM12
#define BOARD_LMT85_SAMPLING_TIM_CLK_SOURCE			LL_TIM_CLOCKSOURCE_INTERNAL
#define BOARD_LMT85_SAMPLING_TIM_IRQn				TIM8_BRK_TIM12_IRQn
#define Board_LMT85_Sampling_TIM_IRQHandler			TIM8_BRK_TIM12_IRQHandler /* !< Re-definition of the ISR handler. Is implemented in the driver itself. */
#define BOARD_LMT85_SAMPLING_TIM_ISR_FREQ			100000 /*!< Sampling frequency of the sensors in Hz.*/
#define BOARD_LMT85_SAMPLES_QTY	(BOARD_LMT85_FIR_FILTER_ORDER*10) /*!< The lmt85 will sample this N times in each call to the board_lmt85_measure_all function. */

/* @brief This IDs are used by the API to identify each sensor. */
enum
{
	BOARD_LMT85_TS_T0 = 0,
	BOARD_LMT85_TS_T1,
	BOARD_LMT85_TS_T2,
	BOARD_LMT85_TS_T3,
	BOARD_LMT85_TS_N0,
	BOARD_LMT85_TS_N1,
	BOARD_LMT85_TS_N2,
	BOARD_LMT85_TS_N3,
	BOARD_LMT85_TS_N4,
	BOARD_LMT85_TS_N5,
	BOARD_LMT85_MAX		/* <! Number of ADC inputs used. */
};

#define BOARD_LMT85_TS_T0_GPORT		GPIOA			/*!< Checked! */
#define BOARD_LMT85_TS_T0_GPIN		GPIO_PIN_3		/*!< Checked! */

#define BOARD_LMT85_TS_T1_GPORT		GPIOA			/*!< Checked! */
#define BOARD_LMT85_TS_T1_GPIN		GPIO_PIN_6		/*!< Checked! */

#define BOARD_LMT85_TS_T2_GPORT		GPIOB			/*!< Checked! */
#define BOARD_LMT85_TS_T2_GPIN		GPIO_PIN_0		/*!< Checked! */

#define BOARD_LMT85_TS_T3_GPORT		GPIOB			/*!< Checked! */
#define BOARD_LMT85_TS_T3_GPIN		GPIO_PIN_1		/*!< Checked! */

#define BOARD_LMT85_TS_N0_GPORT		GPIOF			/*!< Checked! */
#define BOARD_LMT85_TS_N0_GPIN		GPIO_PIN_3		/*!< Checked! */

#define BOARD_LMT85_TS_N1_GPORT		GPIOF			/*!< Checked! */
#define BOARD_LMT85_TS_N1_GPIN		GPIO_PIN_4		/*!< Checked! */

#define BOARD_LMT85_TS_N2_GPORT		GPIOF			/*!< Checked! */
#define BOARD_LMT85_TS_N2_GPIN		GPIO_PIN_6		/*!< Checked! */

#define BOARD_LMT85_TS_N3_GPORT		GPIOF			/*!< Checked! */
#define BOARD_LMT85_TS_N3_GPIN		GPIO_PIN_5		/*!< Checked! */

#define BOARD_LMT85_TS_N4_GPORT		GPIOF			/*!< Checked! */
#define BOARD_LMT85_TS_N4_GPIN		GPIO_PIN_7		/*!< Checked! */

#define BOARD_LMT85_TS_N5_GPORT		GPIOF			/*!< Checked! */
#define BOARD_LMT85_TS_N5_GPIN		GPIO_PIN_8		/*!< Checked! */


#endif /* BOARD_CONFIG_OTB_C_ */
