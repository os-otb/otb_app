/*
 * board_config.h
 *
 *  Created on: Aug 5, 2021
 *      Author: julian alias Marifante
 *		Email: jnrodriguezz@hotmail.com
 *		Gitlab: https://gitlab.com/Marifante
 *		Github: https://github.com/Marifante
 */

#ifndef BOARD_CONFIG_H_
#define BOARD_CONFIG_H_

#include "project.h"

#ifdef OTB
#include "board_config_otb.h"
#endif

#endif /* BOARD_CONFIG_H_ */
