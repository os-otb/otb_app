/*
 * board_config_os_obc.c
 *
 *  Created on: Aug 29, 2021
 *      Author: julian alias Marifante
 *		Email: jnrodriguezz@hotmail.com
 *		Gitlab: https://gitlab.com/Marifante
 *		Github: https://github.com/Marifante
 */


#include "project.h"
#include "api.h"

#ifdef OTB
#include "api_config_otb.h"
#include "board_config_otb.h"

/* Configuration needed by the API to distinguish each temp 
 * sensor of the board and classify them with a TAG.. */
const api_temp_sensor_t api_temp_sensor[] = 
{
	API_TEMP_SENSOR_ID_TAG(BOARD_LMT85_TS_T0, "T0"),
	API_TEMP_SENSOR_ID_TAG(BOARD_LMT85_TS_T1, "T1"),
	API_TEMP_SENSOR_ID_TAG(BOARD_LMT85_TS_T2, "T2"),
	API_TEMP_SENSOR_ID_TAG(BOARD_LMT85_TS_T3, "T3"),
	API_TEMP_SENSOR_ID_TAG(BOARD_LMT85_TS_N0, "N0"),
	API_TEMP_SENSOR_ID_TAG(BOARD_LMT85_TS_N1, "N1"),
	API_TEMP_SENSOR_ID_TAG(BOARD_LMT85_TS_N2, "N2"),
	API_TEMP_SENSOR_ID_TAG(BOARD_LMT85_TS_N3, "N3"),
	API_TEMP_SENSOR_ID_TAG(BOARD_LMT85_TS_N4, "N4"),
	API_TEMP_SENSOR_ID_TAG(BOARD_LMT85_TS_N5, "N5"),
};

#endif
