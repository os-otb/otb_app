/*
 * api_config.h
 *
 *  Created on: Jul 18, 2021
 *      Author: marifante
 */

#ifndef API_CONFIG_OTB_H_
#define API_CONFIG_OTB_H_

#include "api_version.h"

/* Used to define the base address of the flash where the code compiled will be stored. */
#define API_CONFIG_CODE_BASE_ADDRESS	0x08040000

// Logger ---------------------------------------------------------------------
#define API_LOGGER_INITIAL_MSG_TITLE "------------------------------------ OTB APP ------------------------------------------\r\nAPI_VER=" API_VERSION

// File system ----------------------------------------------------------------
/* Use FreeRTOS+FAT. */
#define API_FS_USE_FF_FAT	/*!< Use FreeRTOS+FAT to implement a filesystem. */
#define API_FS_USE_MB85RS	/*!< Use MB85RS FRAM memories. The hardware config must be set in the board config. */

// FTP config -----------------------------------------------------------------
#define API_FTP_SERVER_ROOT_DIR_DISK_PATH API_FS_FRAM0_DISK_PATH
#define API_FTP_SERVER_ROOT_DIR	API_FTP_SERVER_ROOT_DIR_DISK_PATH "/ftp"

// IAP config  ----------------------------------------------------------------
#define API_IAP_APP0_FIRST_SECTOR_LOC	API_FLASH_SECTOR_6	/*!< The first sector where the APP0 will be located. */
#define API_IAP_APP0_SECTORS_LOCATION	((1 << API_IAP_APP0_FIRST_SECTOR_LOC) | (1 << (API_IAP_APP0_FIRST_SECTOR_LOC+1)))
#define API_IAP_APP1_FIRST_SECTOR_LOC	API_FLASH_SECTOR_8	/*!< The first sector where the APP1 will be located. */
#define API_IAP_APP1_SECTORS_LOCATION	((1 << API_IAP_APP1_FIRST_SECTOR_LOC) | (1 << (API_IAP_APP1_FIRST_SECTOR_LOC+1)))
#define API_IAP_BIN_DISK_PATH	API_FS_FRAM0_DISK_PATH	/*!< The disk where the bin is stored. */
#define API_IAP_BIN_DIR_PATH	API_FTP_SERVER_ROOT_DIR /*!< The binary must be located in the root dir of the ftp server. */
#define API_IAP_APP0_FILENAME	"app0.bin" /*!< The name of the binary corresponding to the app0. */
#define API_IAP_APP1_FILENAME	"app1.bin" /*!< The name of the binary corresponding to the app1. */
#define API_IAP_APP0_ABS_PATH	API_IAP_BIN_DIR_PATH "/" API_IAP_APP0_FILENAME /*!< The absolute path of the binary of the app0. */
#define API_IAP_APP1_ABS_PATH	API_IAP_BIN_DIR_PATH "/" API_IAP_APP1_FILENAME /*!< The absolute path of the binary of the app1. */

/* API default config. This must be here to fill the configurations that has been left. */
#include "api_config_default.h"

#endif /* API_CONFIG_OTB_H_ */
