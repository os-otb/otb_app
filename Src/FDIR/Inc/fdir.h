/*
 * boot_fdir.h
 *
 *  Created on: Aug 16, 2021
 *      Author: Julian Rodriguez aka Marifante
 *		Email: jnrodriguezz@hotmail.com
 *		Gitlab: https://gitlab.com/Marifante
 *		Github: https://github.com/Marifante
 */

#ifndef FDIR_H_
#define FDIR_H_

// Includes -------------------------------------------------------------------
#include "api_types.h"
#include "rtos.h"

// Public functions declaration -----------------------------------------------
TaskHandle_t fdir_init(void *arg);

#endif /* BOOT_FDIR_H_ */
