/*
 * app_private.h
 *
 *  Created on: Jun 6, 2021
 *      Author: Julian Rodriguez aka Marifante 
 *		Email: jnrodriguezz@hotmail.com
 *		Gitlab: https://gitlab.com/Marifante
 *		Github: https://github.com/Marifante
 */

#ifndef APP_PRIVATE_H_
#define APP_PRIVATE_H_

// Includes -------------------------------------------------------------------
/* Tasks modules includes. */
#include "app.h"
#include "net.h"
#include "sec_exp.h"

#include "api.h"	/* !< OBCs API include. */

// Macros and constants -------------------------------------------------------
/* Appwork config. TODO move to another place? */

#define APP_LOG_ENABLED			1		/*<! Enables module log. */

#define	LOG_TAG						"app"	/*<! Tag assigned to logs for this module. */

#if APP_LOG_ENABLED
	#define APP_LOG_ERROR(format, ...)		LOG_ERROR(LOG_TAG, format, ##__VA_ARGS__)
	#define APP_LOG_WARNING(format, ...)	LOG_WARNING(LOG_TAG, format, ##__VA_ARGS__)
	#define APP_LOG_INFO(format, ...)		LOG_INFO(LOG_TAG, format, ##__VA_ARGS__)
	#define APP_LOG_DEBUG(format, ...)		LOG_DEBUG(LOG_TAG, format, ##__VA_ARGS__)
#else
	#define APP_LOG_ERROR(...)
	#define APP_LOG_WARNING(...)
	#define APP_LOG_INFO(...)
	#define APP_LOG_DEBUG(...)
#endif

// Private variables declaration ----------------------------------------------
typedef struct
{
	TaskHandle_t task_handle;
	QueueHandle_t cmd_queue;  	/* !< Other tasks or ISR will notify app task about some events with this queue. */
	SemaphoreHandle_t mutex; /* !< To make some operations in a fast way, another tasks of the system will want to acced to this struct, so we implement a mutex here.*/

	/* This struct have the extended vars of the states of the fsm. With this vars, certain
	 * states of the fsm will do some operations to evaluate the flow of the operation. */
	struct
	{
		struct
		{
			struct
			{
				bool osotb_up;	/*!< To store when the osotb server is up!. */
				bool ftp_up;	/*!< To store when the ftp server is up!. */
			}sv_status;
			int entry_time; /*!< To take in count the time from the most recent entry in the state. */
		}init;
		struct
		{
			int entry_time; /*!< To take in count the time from the most recent entry in the state. */
			bool exp_finished; /* !< Set to true when the experiment task notifies that the experiment is done! */
		}data_collection;
		struct
		{
			int entry_time; /*!< To take in count the time from the most recent entry in the state. */
		}data_transfer;
	}fsm_extended_vars;
	api_fsm_t fsm;	/* !< Struct that holds the information of fsm of this module. */
} app_t;

typedef struct
{
  uint8_t id;
  void (*handler)(app_msg_t *msg);
}app_process_msg_table_t;

// Private variables definition -----------------------------------------------
app_t app;

// Private functions declaration ----------------------------------------------
void app_process_msg(app_msg_t *msg);

// Module finite state machine declarations -----------------------------------
typedef enum
{
	APP_STATE_INIT = 0,
	APP_STATE_IDLE,
	APP_STATE_DATA_COLLECTION,
	APP_STATE_DATA_TRANSFER,
	APP_MAX_STATES,	/*!< Max states. */
}app_fsm_state_id_t;

/*
 * @brief events that will trigger transitions in this fsm:
 * */
typedef enum
{
    APP_EV_DEV_CONN = 0,		/*!< The device is connected to the network and the servers are created. */
	APP_EV_RETRY_INIT,			/*!< Retry the init process... */
	APP_EV_OP_MODE_CMD_DATA_COLLECTION,	/*!< Command to execute the experiment was received!... */
	APP_EV_OP_MODE_CMD_DATA_TRANSFER,	/*!< Command to execute a transference of reports was received!... */
	APP_EV_OP_MODE_CMD_IDLE, /*!< Command to execute a transference of reports was received!... */
	APP_EV_EXPERIMENT_TIMEOUT,	/*!< Event generated when an execution time of an experiment has reached its timeout... */
	APP_EV_EXPERIMENT_FINISHED,	/*!< Event generated when an execution time of an experiment has reached its timeout... */
	APP_EV_TRANSFERS_TIMEOUT,	/*!< Event generated when the DATA TRANSFER mode timeouts... */
	APP_MAX_EVENTS			/*!< Max events on the fsm. */
}app_fsm_state_events_t;

/* APP_STATE_INIT */
#define APP_INIT_STATE_RETRY_TIMEOUT_MS 5000
ret_code_t app_fsm_init_on_entry_handler(void *arg);
ret_code_t app_fsm_init_ongoing_handler(void *arg);

/* APP_STATE_DATA_COLLECTION */
#define APP_INIT_DATA_COLLECTION_DEFAULT_EXP_TIMEOUT_MS 60000 /* !< Timeout for the experiment! */
ret_code_t app_fsm_data_collection_on_entry_handler(void *arg);
ret_code_t app_fsm_data_collection_ongoing_handler(void *arg);

/* APP_STATE_DATA_TRANSFER */
#define APP_INIT_DATA_TRANSFER_TIMEOUT_MS 60000 /* !< Timeout for the data transfer mode! */
ret_code_t app_fsm_data_transfer_ongoing_handler(void *arg);
ret_code_t app_fsm_data_transfer_on_entry_handler(void *arg);

#endif /* APP_PRIVATE_H_ */
