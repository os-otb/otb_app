/*
 * app.c
 *
 *  Created on: Jun 6, 2021
 *      Author: Julian Rodriguez aka Marifante
 *		Email: jnrodriguezz@hotmail.com
 *		Gitlab: https://gitlab.com/Marifante
 *		Github: https://github.com/Marifante
 */

// Include --------------------------------------------------------------------
#include "app_private.h" 	/* !< App module private include. */

// Macros & constants ---------------------------------------------------------
#define APP_TASK_DELAY_MS			50			/*<! in ms, delay of the task. */
#define APP_TASK_STACK_SIZE		512
#define APP_TASK_PRIOTY			osPriorityNormal
#define APP_QUEUE_TIMEOUT			(10 / portTICK_PERIOD_MS)
#define APP_QUEUE_LEN				10

// Private functions declaration ----------------------------------------------
void app_task(void *arg);

// Private variables definition -----------------------------------------------
typedef struct
{
	struct
	{
		app_fsm_state_id_t id;
		char str[APP_OPERATION_MODES_STR_MAX_LEN];
	};
}app_op_mode_str_t;

/* Table that will hold a match between the ids of the states and strings that represent them.
 * Each state will be an operation mode of the device. */
const app_op_mode_str_t op_mode_table[] =
{
		{.id = APP_STATE_INIT, .str = APP_OP_MODE_INIT_STR},
		{.id = APP_STATE_IDLE, .str = APP_OP_MODE_IDLE_STR},
		{.id = APP_STATE_DATA_COLLECTION, .str = APP_OP_MODE_DATA_COLLECTON_STR},
		{.id = APP_STATE_DATA_TRANSFER, .str = APP_OP_MODE_DATA_TRANSFER_STR},
};

/* States of the fsm of this module: */
api_fsm_state_t app_fsm_states[] =
{
	API_FSM_DEFINE_STATE(	APP_STATE_INIT,
							app_fsm_init_on_entry_handler,
							app_fsm_init_ongoing_handler,
							NULL),
	API_FSM_DEFINE_STATE(	APP_STATE_IDLE,
							NULL,
							NULL,
							NULL),
	API_FSM_DEFINE_STATE(	APP_STATE_DATA_COLLECTION,
							app_fsm_data_collection_on_entry_handler,
							app_fsm_data_collection_ongoing_handler,
							NULL),
	API_FSM_DEFINE_STATE(	APP_STATE_DATA_TRANSFER,
							app_fsm_data_transfer_on_entry_handler,
							app_fsm_data_transfer_ongoing_handler,
							NULL),
};

/* Transitions of the fsm of this module: */
api_fsm_transition_t app_fsm_transitions_map[] =
{
	API_FSM_DEFINE_TRANSITION_MAP_ITEM(APP_STATE_INIT, APP_EV_RETRY_INIT, APP_STATE_INIT),
	API_FSM_DEFINE_TRANSITION_MAP_ITEM(APP_STATE_INIT, APP_EV_DEV_CONN, APP_STATE_IDLE),
	API_FSM_DEFINE_TRANSITION_MAP_ITEM(APP_STATE_IDLE, APP_EV_OP_MODE_CMD_DATA_COLLECTION, APP_STATE_DATA_COLLECTION),
	API_FSM_DEFINE_TRANSITION_MAP_ITEM(APP_STATE_IDLE, APP_EV_OP_MODE_CMD_DATA_TRANSFER, APP_STATE_DATA_TRANSFER),
	API_FSM_DEFINE_TRANSITION_MAP_ITEM(APP_STATE_DATA_COLLECTION, APP_EV_EXPERIMENT_TIMEOUT, APP_STATE_IDLE),
	API_FSM_DEFINE_TRANSITION_MAP_ITEM(APP_STATE_DATA_COLLECTION, APP_EV_EXPERIMENT_FINISHED, APP_STATE_IDLE),
	API_FSM_DEFINE_TRANSITION_MAP_ITEM(APP_STATE_DATA_TRANSFER, APP_EV_TRANSFERS_TIMEOUT, APP_STATE_IDLE),
	API_FSM_DEFINE_TRANSITION_MAP_ITEM(APP_STATE_DATA_TRANSFER, APP_EV_OP_MODE_CMD_IDLE, APP_STATE_IDLE),
};

// Public functions definition  -----------------------------------------------
/**
 * @brief init app module of bootloader.
 * @return RET_OK if success.
 * */
TaskHandle_t app_init(void *arg)
{
	/* Init fsm struct: */
	app.fsm.actual_state = &app_fsm_states[0];
	app.fsm.previous_state = &app_fsm_states[0];
	app.fsm.state_table = app_fsm_states;
	app.fsm.transition_map = app_fsm_transitions_map;
	app.fsm.states_qty = APP_MAX_STATES;
	app.fsm.transitions_qty = APP_MAX_EVENTS;

	/* Create commands queue and task: */
	MUTEX_INIT(app.mutex);
	app.cmd_queue = xQueueCreate(APP_QUEUE_LEN, sizeof(app_msg_t));
	if( (NULL != app.cmd_queue) && (NULL != app.mutex) )
	{
		if( xTaskCreate(	app_task, "app",
							APP_TASK_STACK_SIZE,
							NULL,
							APP_TASK_PRIOTY,
							&app.task_handle ) != pdPASS )
		{
			/* If the task can't be created the task handle will be NULL. */
			app.task_handle = NULL;
		}
	}
	return app.task_handle;
}

/**
 * @brief this function is used by another tasks to query the actual operation mode of the device.
 * @param op_mode_str buffer where will be stored a string representing the operation mode.
 * @return RET_OK if success. RET_ERR_INVALID_PARAMS if the string didn't represent nothing!
 * @details this was implementd because we don't want that another tasks (the OSOTB taks) delays
 * too much acceding to the resource. We want a fast reply to the host.
 */
ret_code_t app_get_op_mode_string(char *op_mode_str)
{
	if( NULL == op_mode_str ) return RET_ERR_NULL_POINTER;
	ret_code_t ret = RET_ERR_INVALID_PARAMS;
	MUTEX_UNLOCK(app.mutex);
	for( int c = 0; c < APP_MAX_STATES; c++)
	{
		if( op_mode_table[c].id == app.fsm.actual_state->id )
		{
			strcpy(op_mode_str, op_mode_table[c].str);
			ret = RET_OK;
			break;
		}
	}
	MUTEX_LOCK(app.mutex);
	return ret;
}

/**
 * @brief check if the string op_mode_str represents a valid operation mode.
 * @param op_mode_str the string to check if it is a valid op mode.
 * @details check that a mutex is not needed here because the table op_mode_table is a const value
 * and never will be written!.
 * @return true if it is a valid operation mode or false otherwise.
 */
bool app_represents_valid_op_mode(char *op_mode_str)
{
	if( NULL == op_mode_str ) return false;
	bool ret = false;
	for( int c = 0; c < APP_MAX_STATES; c++)
	{
		if( 0 == strcmp(op_mode_table[c].str, op_mode_str) )
		{
			ret = true;
			break;
		}
	}
	return ret;
}

/**
 * @brief check if the change to another operation mode with a command is valid based
 * in the actual state of the FSM.
 * @param op_mode_str the target operation mode.
 * @return true if the device cna change its operation mode or false otherwise.
 * TODO: I don't think this function is well elaborated. Maybe we can erase it and only
 * rely in the transitions table?
 */
bool app_can_change_op_mode_with_cmd(char *op_mode_str)
{
	if( NULL == op_mode_str ) return false;
	bool ret = false;
	MUTEX_UNLOCK(app.mutex);
	if( APP_STATE_IDLE == app.fsm.actual_state->id ) ret = true;
	if( APP_STATE_DATA_TRANSFER == app.fsm.actual_state->id ) ret = true; // In data tranfer, we can change to idle!
	MUTEX_LOCK(app.mutex);
	return ret;
}

// Private functions definition -----------------------------------------------
void app_task(void *arg)
{
	app_msg_t msg;
	if( 0 != API_FS_mount(APP_TRANSFERS_DISK) )
	{
		APP_LOG_ERROR("%s: cant mount %s",__func__, APP_TRANSFERS_DISK);
	}
	else
	{
		if( 0 != API_FS_mkdir(APP_TRANSFERS_FOLDER) )
		{
			APP_LOG_INFO("%s: can't create %s",__func__, APP_TRANSFERS_FOLDER);
		}
	}

	while(true)
	{
		if(pdTRUE == xQueueReceive(app.cmd_queue, (uint8_t*)&msg, APP_QUEUE_TIMEOUT))
		{
			app_process_msg(&msg); // process incoming message
		}
		api_fsm_work(&app.fsm, NULL); // Process the actual state of the fsm
	}
}

/* Definition of the handlers of the fsm: */
/* APP_FSM_STATE_INIT functions ----------------------------------------*/

/*
 * @brief sends a message to the net task saying it must init the ftp server!
 * */
ret_code_t app_fsm_init_on_entry_handler(void *arg)
{
	ret_code_t ret = RET_OK;
	app.fsm_extended_vars.init.entry_time = TICKS_TO_MS(xTaskGetTickCount());

	ret = net_send_msg_query_sv_status(NET_FTP_SERVER_ID);
	if( RET_OK != ret )
	{
		APP_LOG_ERROR("%s: error sending message to net ftp task");
	}

	ret = net_send_msg_query_sv_status(NET_OSOTB_SERVER_ID);
	if( RET_OK != ret )
	{
		APP_LOG_ERROR("%s: error sending message to net osotb task");
	}

	if( (true == app.fsm_extended_vars.init.sv_status.ftp_up) &&
		(true == app.fsm_extended_vars.init.sv_status.osotb_up) )
	{
		APP_LOG_INFO("%s: The servers are up! continue.", __func__);
		api_fsm_process_event(&app.fsm, APP_EV_DEV_CONN, NULL);
	}

	return ret;
}

/*
 * @brief if the timeout has been reached, retry the init process. An event
 * triggered from the net task will send this fsm to the idle state.
 * */
ret_code_t app_fsm_init_ongoing_handler(void *arg)
{
	ret_code_t ret = RET_OK;
	int now = TICKS_TO_MS(xTaskGetTickCount());
	if( APP_INIT_STATE_RETRY_TIMEOUT_MS < (now - app.fsm_extended_vars.init.entry_time) )
	{
		ret = api_fsm_process_event(&app.fsm, APP_EV_RETRY_INIT, NULL);
	}
	return ret;
}

/* APP_FSM_STATE_DATA_COLLECTION functions ----------------------------------------*/
/*
 * @brief sends a message to the experiment tasks saying them that they need to
 * start to execute the experiments!
 * */
ret_code_t app_fsm_data_collection_on_entry_handler(void *arg)
{
	ret_code_t ret = RET_OK;
	APP_LOG_INFO("%s: starting experiment!", __func__);
	app.fsm_extended_vars.data_collection.exp_finished = false; // reset flag
	app.fsm_extended_vars.data_collection.entry_time = TICKS_TO_MS(xTaskGetTickCount());
	sec_exp_start_parameters_t exp_params = {0};
	ret = sec_exp_send_msg_start(exp_params); // TODO: We need to doy a retry or something here?
	return ret;
}

/*
 * @brief if the timeout has been reached, retry the init process. An event
 * triggered from the net task will send this fsm to the idle state.
 * */
ret_code_t app_fsm_data_collection_ongoing_handler(void *arg)
{
	ret_code_t ret = RET_OK;
	int now = TICKS_TO_MS(xTaskGetTickCount());
	if( true == app.fsm_extended_vars.data_collection.exp_finished )
	{	/* The experiment has finished!! */
		APP_LOG_INFO("%s: experiment finished!", __func__);
		api_fsm_process_event(&app.fsm, APP_EV_EXPERIMENT_FINISHED, NULL);
	}
	else
	if( APP_INIT_DATA_COLLECTION_DEFAULT_EXP_TIMEOUT_MS < (now - app.fsm_extended_vars.data_collection.entry_time) )
	{	/* Timeout has been reached! */
		ret = sec_exp_send_msg_halt();
		if( RET_ERR == ret )
		{
			APP_LOG_ERROR("%s: could not send a message to seconday experiment task!",__func__);
		}
		api_fsm_process_event(&app.fsm, APP_EV_EXPERIMENT_TIMEOUT, NULL);
	}
	return ret;
}

/* APP_FSM_STATE_DATA_TRANSFER functions ----------------------------------------*/
/*
 * @brief sends a message to the experiment tasks saying them that they need to
 * start to execute the experiments!
 * */
ret_code_t app_fsm_data_transfer_on_entry_handler(void *arg)
{
	ret_code_t ret = RET_OK;
	APP_LOG_INFO("%s: starting transfer operation mode", __func__);
	app.fsm_extended_vars.data_transfer.entry_time = TICKS_TO_MS(xTaskGetTickCount());
	/* Move the reports on the data_collection folder to the data_transfer folder. */

	// Here we must to move all the files from the DATA_COLLECTION folder to the DATA_TRANSFER folder.
	return ret;
}

/*
 * @brief if the timeout has been reached, retry the init process. An event
 * triggered from the net task will send this fsm to the idle state.
 * */
ret_code_t app_fsm_data_transfer_ongoing_handler(void *arg)
{
	ret_code_t ret = RET_OK;
	int now = TICKS_TO_MS(xTaskGetTickCount());
	if( APP_INIT_DATA_TRANSFER_TIMEOUT_MS < (now - app.fsm_extended_vars.data_transfer.entry_time) )
	{	/* Timeout has been reached! */
		api_fsm_process_event(&app.fsm, APP_EV_TRANSFERS_TIMEOUT, NULL);
	}
	return ret;
}
