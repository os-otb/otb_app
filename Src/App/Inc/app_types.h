/*
 * app_types.h
 *
 *  Created on: Aug 19, 2021
 *      Author: marifante
 */

#ifndef APP_TYPES_H_
#define APP_TYPES_H_

// Macros & Constants ---------------------------------------------------------
#define APP_OPERATION_MODES_STR_MAX_LEN	16	/*!< Maximun length of a string that represents an operation mode. */
#define APP_OP_MODE_INIT_STR "init"
#define APP_OP_MODE_IDLE_STR "idle"
#define APP_OP_MODE_DATA_COLLECTON_STR "data_collection"
#define APP_OP_MODE_DATA_TRANSFER_STR "data_transfer"

/* The device stores the data in a folder when is collected (in DATA_COLLECTION operation mode) and
 * then moves this files to another folder when the operation mode is changed to DATA_TRANSFER.
 * The host can search the reports in the APP_TRANSFERS_FOLDER. */
#define APP_TRANSFERS_DISK				API_FS_FRAM0_DISK_PATH
#define APP_TRANSFERS_FOLDER 			API_FTP_SERVER_ROOT_DIR "/transfers"

// Public variable declaration ------------------------------------------------
typedef enum
{
	APP_MSG_DEFAULT = 0,
	APP_MSG_NET_SV_STATUS,	 	/*!< Message sent by the a net server task with the server status!. */
	APP_MSG_CMD_CHANGE_OP_MODE, /*!< Message sent by a net server task to indicate that a change operation mode command has been received. */
}app_msg_id_t;

/* @brief struct sent by a server task to the app task with the status of the server. */
typedef struct
{
	net_server_id_t sv_id;
	bool sv_up;		/*!< The app only wants to know if the server is up (true) or if it is down (false). */
} app_msg_sv_status_t;

/**
 * @brief message packet format sent to the boot app task.
 * The data field is an union with the different data packets that can be sent
 * to the task.
 */
typedef struct //STRUCT_PACKET
{
	app_msg_id_t id;
	union
	{
		app_msg_sv_status_t sv_status;
		char target_op_mode[APP_OPERATION_MODES_STR_MAX_LEN];
	}data;
}app_msg_t;


#endif /* APP_INC_APP_TYPES_H_ */
