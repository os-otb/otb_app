/*
 * boot_app.h
 *
 *  Created on: May 19, 2021
 *      Author: marifante
 */

#ifndef APP_H_
#define APP_H_

// Include --------------------------------------------------------------------
#include "api_types.h"
#include "rtos.h"

// Bootloader modules types
#include "net_types.h"
#include "app_types.h"

// Public functions declaration  ----------------------------------------------
TaskHandle_t app_init(void *arg);

/* Functions used by other tasks to send msgs to the app task. */
ret_code_t app_send_msg_sv_status(app_msg_sv_status_t sv_status); /* !< Used by a server task to send information about it status to the app task. */

/* Misc functions used by another tasks. */
ret_code_t app_get_op_mode_string(char *op_mode_str);	/*!< Used by another tasks to get a string with the actual operation mode. */
bool app_represents_valid_op_mode(char *op_mode_str); /*!< Used by another tasks to check if a operation mode is valid. */
bool app_can_change_op_mode_with_cmd(char *op_mode_str); /* !< Used by server task to check if the device can change its operation mode based in the actual one. */
ret_code_t app_send_msg_change_op_mode_cmd(char *target_op_mode);	/*!< Used by a server task to indicate that an operation mode change command has been received. */

#endif /* BOOT_APP_H_ */
