/*
 * app_msg.c
 *
 *  Created on: Jun 6, 2021
 *      Author: Julian Rodriguez aka Marifante 
 *		Email: jnrodriguezz@hotmail.com
 *		Gitlab: https://gitlab.com/Marifante
 *		Github: https://github.com/Marifante
 */

// Include --------------------------------------------------------------------
#include "app_private.h" 	/* !< App module private include. */

// Private variables declaration ----------------------------------------------
static ret_code_t app_send_msg(app_msg_t *msg);
/* App Task Messages handlers prototypes: */
static void app_process_msg_sv_status(app_msg_t *msg);
static void app_process_msg_change_op_mode_cmd(app_msg_t *msg);

// Boot App Task message-handler table ----------------------------------------
/* App Task Messages handlers table: */
#define APP_PROCESS_MSG(x,y) {.id = x, .handler=y}
static const app_process_msg_table_t app_process_msg_table[] =
{
	APP_PROCESS_MSG(APP_MSG_DEFAULT, NULL),
	APP_PROCESS_MSG(APP_MSG_NET_SV_STATUS, app_process_msg_sv_status),
	APP_PROCESS_MSG(APP_MSG_CMD_CHANGE_OP_MODE, app_process_msg_change_op_mode_cmd),
};
#define SIZEOF_APP_PROCESS_MSG_TABLE (sizeof(app_process_msg_table) / sizeof(app_process_msg_table[0]))

// Public functions definition  -----------------------------------------------
/*
 * @brief used by a net server task to send the status of the server to the
 * app task.
 * */
ret_code_t app_send_msg_sv_status(app_msg_sv_status_t sv_status)
{
	app_msg_t msg;
	msg.data.sv_status.sv_id = sv_status.sv_id;
	msg.data.sv_status.sv_up = sv_status.sv_up;
	msg.id = APP_MSG_NET_SV_STATUS;
	return app_send_msg(&msg);
}

/*
 * @brief used by a net server task to indicate that a command to change
 * operation mode has been received.
 * @param target_op_mode a target to an operation mode string.
 * */
ret_code_t app_send_msg_change_op_mode_cmd(char *target_op_mode)
{
	if( NULL == target_op_mode ) return RET_ERR_NULL_POINTER;
	app_msg_t msg;
	strcpy(msg.data.target_op_mode, target_op_mode);
	msg.id = APP_MSG_CMD_CHANGE_OP_MODE;
	return app_send_msg(&msg);
}

/**
 * @brief send a message to the app task through its queue.
 * This function can be called from an ISR too.
 * */
static ret_code_t app_send_msg(app_msg_t *msg)
{
	BaseType_t rtos_result = pdTRUE;
	ret_code_t res = RET_OK;

	if( (NULL == app.cmd_queue) || (NULL == msg) )
	{
		return RET_ERR;
	}
	else
	{
		if(pdTRUE != xPortIsInsideInterrupt())
		{
			/* If the core isn't in an interrupt context then send the msg normally. */
			rtos_result = xQueueSend( app.cmd_queue, msg, ( TickType_t ) 10 );
		}
		else
		{
				/* If the core is in an interrupt context then use send from ISR API. */
			portBASE_TYPE xHigherPriorityTaskWoken;
			BaseType_t yield_req = pdFALSE;

			rtos_result = xQueueSendFromISR( app.cmd_queue, msg, &xHigherPriorityTaskWoken );
			// Now the buffer is empty we can switch context if necessary.
			if( xHigherPriorityTaskWoken )
			{
			  // Actual macro used here is port specific.
			  portYIELD_FROM_ISR(yield_req);
			}
		}
		res = (pdTRUE == rtos_result) ? RET_OK : RET_ERR;
		return res;
	}
}

// Private functions definition -----------------------------------------------
/**
 *
 * @brief  process incoming queue messages. This function calls the handler
 * associated with that message.
 *
 * @param msg queue message
 *
 */
void app_process_msg(app_msg_t *msg)
{
	uint8_t c;
	if( NULL != msg )
	{
		for(c = 0; c < SIZEOF_APP_PROCESS_MSG_TABLE; c++)
		{
			if(app_process_msg_table[c].id == msg->id)
			{
				// found, check callback
				if(NULL != app_process_msg_table[c].handler)
				{
					// call it
					app_process_msg_table[c].handler(msg);
				}
				// finish search
				break;
			}
		}
	}
}

/**
 * @brief another task advised to app task that the device is connected
 * to the network!
 * */
static void app_process_msg_sv_status(app_msg_t *msg)
{
	if( NULL != msg )
	{
		if( (NET_FTP_SERVER_ID == msg->data.sv_status.sv_id ) &&
			(true == msg->data.sv_status.sv_up))
		{
			 app.fsm_extended_vars.init.sv_status.ftp_up = true;
		}

		if( (NET_OSOTB_SERVER_ID == msg->data.sv_status.sv_id ) &&
			(true == msg->data.sv_status.sv_up))
		{
			 app.fsm_extended_vars.init.sv_status.osotb_up = true;
		}
	}
	else
	{
		APP_LOG_ERROR("%s: msg is null!", __func__);
	}
}

/**
 * @brief another task advised to app task that a command to change the
 * operation mode has been received.
 * */
static void app_process_msg_change_op_mode_cmd(app_msg_t *msg)
{
	if( NULL != msg )
	{
		if( strcmp(msg->data.target_op_mode, APP_OP_MODE_DATA_COLLECTON_STR) == 0 )
		{
			api_fsm_process_event(&app.fsm, APP_EV_OP_MODE_CMD_DATA_COLLECTION, NULL); // TODO here we can pass the arguments of the experiment!
		}
		else
		if( strcmp(msg->data.target_op_mode, APP_OP_MODE_DATA_TRANSFER_STR) == 0 )
		{
			api_fsm_process_event(&app.fsm, APP_EV_OP_MODE_CMD_DATA_TRANSFER, NULL); // TODO here we can pass the arguments of the experiment!
		}
		else
		if( strcmp(msg->data.target_op_mode, APP_OP_MODE_IDLE_STR) == 0 )
		{
			api_fsm_process_event(&app.fsm, APP_EV_OP_MODE_CMD_IDLE, NULL);
		}
	}
	else
	{
		APP_LOG_ERROR("%s: msg is null!", __func__);
	}
}
