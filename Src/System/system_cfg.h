/*
 * system_cfg.h
 *
 *  Created on: May 1, 2021
 *      Author: Julian Rodriguez aka Marifante 
 *		Email: jnrodriguezz@hotmail.com
 *		Gitlab: https://gitlab.com/Marifante
 *		Github: https://github.com/Marifante
 */

#ifndef SYSTEM_CFG_H_
#define SYSTEM_CFG_H_

// Include --------------------------------------------------------------------
#include "system.h"
/* Tasks modules includes. */
#include "app.h"
#include "net.h"
#include "fdir.h"
#include "sec_exp.h"

#include "api.h"	/* !< OBCs API include. */

// Macros & constants  --------------------------------------------------------

// Public variables declaration  ----------------------------------------------
typedef struct
{
  TaskHandle_t (*init_handler)(void *arg);
  void *init_argument;		/*!< Argument passed to initializing function. */
}system_handler_t;

// Public variables definition  -----------------------------------------------
static const system_handler_t system_handler_table[] =
{
  {.init_handler = API_Logger_Init, .init_argument = NULL},			/*!< SYSTEM_TASK_LOGGER. */
  {.init_handler = fdir_init, .init_argument = NULL},			/*!< SYSTEM_TASK_FDIR. */
  {.init_handler = app_init, .init_argument = NULL},			/*!< SYSTEM_TASK_APP. */
  {.init_handler = net_init, .init_argument = NULL},			/*!< SYSTEM_TASK_NET. */
  {.init_handler = sec_exp_init, .init_argument = NULL},			/*!< SYSTEM_TASK_SEC_EXP. */
};

#define SYSTEM_SIZEOF_TASK_TABLE (sizeof(system_handler_table) / sizeof(system_handler_table[0]))
#endif /* SYSTEM_INC_SYSTEM_CFG_H_ */
