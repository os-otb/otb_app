/*
 * system.c
 *
 *  Created on: May 1, 2021
 *      Author: Julian Rodriguez aka Marifante 
 *		Email: jnrodriguezz@hotmail.com
 *		Gitlab: https://gitlab.com/Marifante
 *		Github: https://github.com/Marifante
 */

// Include --------------------------------------------------------------------
#include "system_cfg.h" 	/* !< System module private include. */

// Macros & constants ---------------------------------------------------------
#define SYSTEM_LOG_ENABLED			1	/*<! Enables module log. */

#define	LOG_TAG							"sys"	/*<! Tag assigned to logs for this module. */

#if SYSTEM_LOG_ENABLED
	#define SYSTEM_LOG_ERROR(format, ...)		LOG_ERROR(LOG_TAG, format, ##__VA_ARGS__)
	#define SYSTEM_LOG_WARNING(format, ...)		LOG_WARNING(LOG_TAG, format, ##__VA_ARGS__)
	#define SYSTEM_LOG_INFO(format, ...)		LOG_INFO(LOG_TAG, format, ##__VA_ARGS__)
	#define SYSTEM_LOG_DEBUG(format, ...)		LOG_DEBUG(LOG_TAG, format, ##__VA_ARGS__)
#else
	#define SYSTEM_LOG_ERROR(...)
	#define SYSTEM_LOG_WARNING(...)
	#define SYSTEM_LOG_INFO(...)
	#define SYSTEM_LOG_DEBUG(...)
#endif

// Private variables declaration ----------------------------------------------
typedef struct
{
	struct
	{
		uint32_t created;						/*<! Number of tasks created. */
		TaskHandle_t handles[SYSTEM_SIZEOF_TASK_TABLE];	/*<! Handles of the tasks created. */
	}tasks;
} sys_t;

// Private variables definition -----------------------------------------------
static sys_t sys;

// Private functions declaration ----------------------------------------------
ret_code_t system_init(void);
bool system_check_condition(void);
void system_force_reset(void);
ret_code_t system_init_api(void);

// Public functions definition  -----------------------------------------------
/**
 * @brief  The entry point of the application.
 * @retval int
 */
int main(void)
{
	ret_code_t ret = RET_OK;

	/* Init sys control struct. */
	memset(&sys, 0, sizeof(sys));

	ret = system_init_api();
	if(ret == RET_OK) system_init();

	/* We should never get here as control is now taken by the scheduler */
	while (1)
	{

	}
}

// Private functions definition  ----------------------------------------------
/*
 * @brief init Boot code.
 * */
ret_code_t system_init(void)
{
	ret_code_t ret = RET_OK;

	for(int c = 0; c < SYSTEM_SIZEOF_TASK_TABLE ; c++)
	{
		if(NULL != system_handler_table[c].init_handler)
		{
		  sys.tasks.handles[sys.tasks.created] = system_handler_table[sys.tasks.created].init_handler(system_handler_table[sys.tasks.created].init_argument);
		  sys.tasks.created++;
		}
	}

	/* Start scheduler */
	vTaskStartScheduler();
	/* NOTE never must to reach this point. */
	return ret;
}

/*
 * @brief forces a system reset.
 * */
void system_force_reset(void)
{
//	NVIC_SystemReset();
}

/**
 * @brief initialize the APIs needed by the Boot process.
 **/
ret_code_t system_init_api(void)
{
	ret_code_t ret = RET_OK;

	/* When the User Code inits the API, the API inits the Board. */
	ret = API_Board_Init();
	if( RET_OK != ret )
	{
		/* TODO Handle error. */
	}

	/* Datetime must be initialized before all the APIs because
	 * its used for them. */
	ret = API_Datetime_Init();
	if( RET_OK != ret )
	{
		/* TODO Handle error. */
	}

	ret = API_FS_Init();
	if( RET_OK != ret )
	{
		/* TODO Handle error. */
	}

	/**
	 * TODO:
	 * - Init FLASH API (verifies that the flash is unprotected).
	 **/
	return ret;
}
